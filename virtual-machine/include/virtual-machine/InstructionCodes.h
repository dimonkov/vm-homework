#ifndef INSTRUCTION_CODES_H_
#define INSTRUCTION_CODES_H_

enum class InstructionCodes : unsigned char
{
    INC  = 0x01,
    DEC  = 0x02,
    MOV  = 0x03,
    MOVC = 0x04,
    LSL  = 0x05,
    LSR  = 0x06,
    JMP  = 0x07,
    JZ   = 0x08,
    JNZ  = 0x09,
    JFE  = 0x0A,
    RET  = 0x0B,
    ADD  = 0x0C,
    SUB  = 0x0D,
    XOR  = 0x0E,
    OR   = 0x0F,
    IN   = 0x10,
    OUT  = 0x11
};

#endif //!INSTRUCTION_CODES_H_
