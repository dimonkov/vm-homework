#ifndef CLI_PARAMETERS_H_
#define CLI_PARAMETERS_H_
#include <string>

class CliParameters
{
public:
	std::string program_file_name;

	static CliParameters ParseCliParameters(int argc, char** argv);
};

#endif // !CLI_PARAMETERS_H_
