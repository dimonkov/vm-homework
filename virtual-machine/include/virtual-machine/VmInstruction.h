#ifndef VM_INSTRUCTION_H_
#define VM_INSTRUCTION_H_
#include "InstructionCodes.h"

struct VmInstruction {
    InstructionCodes code;
    char operands;
};

#endif //!VM_INSTRUCTION_H_
