#ifndef VIRTUAL_MACHINE_H_
#define VIRTUAL_MACHINE_H_

#include <array>
#include <iostream>
#include "InstructionCodes.h"
#include "VmInstruction.h"

class VirtualMachine
{
public:
	static const unsigned int REGISTER_COUNT = 16;
	static const unsigned int PROGRAM_MEMORY_SIZE = 256;

	static const int ZF = 0;
	static const int EOF_F = 1;

	
	VirtualMachine(std::array<char, PROGRAM_MEMORY_SIZE>& program,
		std::istream& input_stream,
		std::ostream& out_data_stream);

	void PerformNextInstruction();
	bool GetFlag(int flagN) const;
	bool get_halted() const;
    
private:
	unsigned char registers[REGISTER_COUNT] = {0};
	std::array<char, PROGRAM_MEMORY_SIZE>& prog_memory;
    bool halted;

	std::istream& input_data_stream;
	std::ostream& output_stream;
    
    VmInstruction* instruction;
    
    int flags;
    
    
    void PerformSpecificInstruction(InstructionCodes code, char operands);
    
    
    inline void INC(unsigned char reg);
    inline void DEC(unsigned char reg);
    inline void MOV(unsigned char reg1, unsigned char reg2);
    inline void MOVC(unsigned char constant);
    inline void LSL(unsigned char reg);
    inline void LSR(unsigned char reg);
    inline void JMP(char offset);
    inline void JZ(char offset);
    inline void JNZ(char offset);
    inline void JFE(char offset);
    inline void RET();
    inline void ADD(unsigned char reg1, unsigned char reg2);
    inline void SUB(unsigned char reg1, unsigned char reg2);
    inline void XOR(unsigned char reg1, unsigned char reg2);
    inline void OR(unsigned char reg1, unsigned char reg2);
    inline void IN(unsigned char reg);
    inline void OUT(unsigned char reg);
    
    inline void SetFlag(int flagN, bool flag_value);
    
    
    
};

#endif //!VIRTUAL_MACHINE_H_
