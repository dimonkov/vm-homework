#include <iostream>
#include "VirtualMachine.h"
#include "InstructionCodes.h"

VirtualMachine::VirtualMachine(std::array<char, PROGRAM_MEMORY_SIZE>& program,
							   std::istream& input_stream,
							   std::ostream& out_data_stream) 
	: prog_memory(program),
	input_data_stream(input_stream),
	output_stream(out_data_stream)
{
    instruction = (VmInstruction*)&prog_memory[0];
	halted = false;
	flags = 0;
}


void VirtualMachine::PerformNextInstruction()
{
    if(halted)
    {
        std::cerr << "VM has already halted, but you are still trying to advance it!!!" << std::endl;
        return;
    }
        
    VmInstruction ins = *instruction;
    PerformSpecificInstruction(ins.code, ins.operands);
}
void VirtualMachine::PerformSpecificInstruction(InstructionCodes code, char operands)
{
    unsigned char op1 = operands & 0x0F, op2 = (operands & 0xF0) >> 4;
    unsigned char operand = static_cast<unsigned char>(operands);
    
    instruction++;
    
    switch (code) {
        case InstructionCodes::INC:
            INC(operand);
            break;
        case InstructionCodes::DEC:
            DEC(operand);
            break;
        case InstructionCodes::MOV:
            MOV(op1, op2);
            break;
        case InstructionCodes::MOVC:
            MOVC(operand);
            break;
        case InstructionCodes::LSL:
            LSL(operand);
            break;
        case InstructionCodes::LSR:
            LSR(operand);
            break;
        case InstructionCodes::JMP:
            JMP(operands);
            break;
        case InstructionCodes::JZ:
            JZ(operands);
            break;
        case InstructionCodes::JNZ:
            JNZ(operands);
            break;
        case InstructionCodes::JFE:
            JFE(operands);
            break;
        case InstructionCodes::RET:
            RET();
            break;
        case InstructionCodes::ADD:
            ADD(op1, op2);
            break;
        case InstructionCodes::SUB:
            SUB(op1, op2);
            break;
        case InstructionCodes::XOR:
            XOR(op1, op2);
            break;
        case InstructionCodes::OR:
            OR(op1, op2);
            break;
        case InstructionCodes::IN:
            IN(operand);
            break;
        case InstructionCodes::OUT:
            OUT(operand);
            break;
        default:
            std::cerr << "Found unknown instruction!" << std::endl;
            break;
    }
}


inline void VirtualMachine::INC(unsigned char reg)
{
    registers[reg] ++;
    SetFlag(VirtualMachine::ZF, registers[reg] == 0);
}
inline void VirtualMachine::DEC(unsigned char reg)
{
    registers[reg] --;
    SetFlag(VirtualMachine::ZF, registers[reg] == 0);
}
inline void VirtualMachine::MOV(unsigned char reg1, unsigned char reg2)
{
    registers[reg1] = registers[reg2];
}
inline void VirtualMachine::MOVC(unsigned char constant)
{
    registers[0] = constant;
}
inline void VirtualMachine::LSL(unsigned char reg)
{
    registers[reg] = registers[reg] << 1;
    SetFlag(VirtualMachine::ZF, registers[reg] == 0);
}
inline void VirtualMachine::LSR(unsigned char reg)
{
    registers[reg] = registers[reg] >> 1;
    SetFlag(VirtualMachine::ZF, registers[reg] == 0);
}
inline void VirtualMachine::JMP(char offset)
{
    instruction = (VmInstruction*)((char*)(instruction) + offset);
	instruction--;
}
inline void VirtualMachine::JZ(char offset)
{
	if (GetFlag(VirtualMachine::ZF))
	{
		instruction = (VmInstruction*)((char*)(instruction)+offset);
		instruction--;
	}
}
inline void VirtualMachine::JNZ(char offset)
{
	if (!GetFlag(VirtualMachine::ZF))
	{
		instruction = (VmInstruction*)((char*)(instruction)+offset);
		instruction--;
	}
}
inline void VirtualMachine::JFE(char offset)
{
	if (GetFlag(VirtualMachine::EOF_F))
	{
		instruction = (VmInstruction*)((char*)(instruction)+offset);
		instruction--;
	}
}
inline void VirtualMachine::RET()
{
    halted = true;
}
inline void VirtualMachine::ADD(unsigned char reg1, unsigned char reg2)
{
    registers[reg1] = registers[reg1] + registers[reg2];
    SetFlag(VirtualMachine::ZF, registers[reg1] == 0);
}
inline void VirtualMachine::SUB(unsigned char reg1, unsigned char reg2)
{
    registers[reg1] = registers[reg1] - registers[reg2];
    SetFlag(VirtualMachine::ZF, registers[reg1] == 0);
}
inline void VirtualMachine::XOR(unsigned char reg1, unsigned char reg2)
{
    registers[reg1] = registers[reg1] ^ registers[reg2];
    SetFlag(VirtualMachine::ZF, registers[reg1] == 0);
}
inline void VirtualMachine::OR(unsigned char reg1, unsigned char reg2)
{
    registers[reg1] = registers[reg1] | registers[reg2];
    SetFlag(VirtualMachine::ZF, registers[reg1] == 0);
}
inline void VirtualMachine::IN(unsigned char reg)
{
	unsigned char input;
	input_data_stream >> input;
	registers[reg] = input;

	if (input_data_stream.eof())
		SetFlag(VirtualMachine::EOF_F, true);
}
inline void VirtualMachine::OUT(unsigned char reg)
{
	output_stream << registers[reg];
}

inline void VirtualMachine::SetFlag(int flagN, bool flag_value)
{
    int flag = static_cast<int>(flag_value) << flagN;
    int mask = 1 << flagN;
    
    flags = (mask & flag)|(~mask & flag);
}

bool VirtualMachine::GetFlag(int flagN) const
{
    int mask = 1 << flagN;
    
    return (flags & mask) != 0;
}

bool VirtualMachine::get_halted() const
{
    return halted;
}
