#include <string>
#include <iostream>
#include <stdlib.h>
#include "cli_parameters.h"

CliParameters CliParameters::ParseCliParameters(int argc, char ** argv)
{
	bool program_file_specified = false;
	CliParameters param;

	for (int i = 0; i < argc; i++)
	{
		std::string arg = std::string(argv[i]);
		if (arg == "-p" && i+1 < argc)
		{
			param.program_file_name = std::string(argv[i + 1]);
			program_file_specified = true;
		}
		if (arg == "-h")
		{
			std::cout << std::endl << "Options:" << std::endl;
			std::cout << "\"-h\" Prints this screen" << std::endl;
			std::cout << "\"-p\" <filename> Allows to specify the program file name" << std::endl;
			exit(EXIT_SUCCESS);
		}
	}

	if (!program_file_specified)
	{
		std::cerr << "No program file specified, please use -p <filename>!" << std::endl;
		exit(EXIT_FAILURE);
	}

	return param;
}
