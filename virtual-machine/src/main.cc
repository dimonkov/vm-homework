#include <iostream>
#include <fstream>
#include "VirtualMachine.h"
#include "cli_parameters.h"

int main(int argc, char** argv)
{
	CliParameters params = CliParameters::ParseCliParameters(argc, argv);
	std::array<char, VirtualMachine::PROGRAM_MEMORY_SIZE> prog;
	std::ifstream prog_data(params.program_file_name.c_str(), std::ios::binary);
	prog_data.read(&prog[0], VirtualMachine::PROGRAM_MEMORY_SIZE);

	VirtualMachine vm(prog, std::cin, std::cout);
	while (!vm.get_halted())
	{
		vm.PerformNextInstruction();
	}
    return 0;
}
