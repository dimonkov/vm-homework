# This is homework project for implementing virtual machine that can run arbitrary programs.

## Building
You'll need to have cmake >= 3.5 installed and added to your path.
Open your terminal in root of this repository and run the following code:

```bash
mkdir build
cd build
cmake ..
```
Built project files will be placed in "build" folder.
Note: If you are on macOS you might want to run cmake .. -G"Xcode" to generate Xcode project.  

### Remember - building in release mode will yield the best performance.  

## Entities

### virtual-machine
This executable spins up a virtual machine that can run user specified binary program (receives data on STDIN, and outputs to STDOUT).  
Allows CLI user interaction.  

Usage Example:  
```bash
$ cat q1_encr.txt | ./virtual-machine.exe -p ./decryptor.bin
```

Help:
```bash
./virtual-machine.exe -h
```

 
